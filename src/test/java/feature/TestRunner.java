package feature;
 
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(
 features = "src/test/java/Feature/ebayFlow.feature"
 ,glue={"stepDefinition"}
 )
 
public class TestRunner {
 
}