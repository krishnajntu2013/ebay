package stepDefinition;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjectModel.CartPage;
import pageObjectModel.DriverInitializer;
import pageObjectModel.HomePage;

public class ebayFlowStepDefinition {
	
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
       HomePage.openHomePage();
		}

	@When("^Search for book and add to cart$")
	public void Search_for_book_and_add_to_cart() throws Throwable {
		HomePage.searchBookAddToCart();
		}

	@And("^verify book added to cart$")
	public void verify_book_added_to_cart() throws Throwable {
		CartPage.verifyItemAddedToCart();
		}

	@And("^Delete the cart$")
	public void Delete_the_cart_() throws Throwable {
		CartPage.deleteItemfromCart();
	}

	@Then("^Verify cart is empty$")
	public void Verify_cart_is_empty() throws Throwable {
		CartPage.verifyCartEmpty();
	}

	
}