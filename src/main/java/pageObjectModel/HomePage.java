package pageObjectModel;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.internal.FindsByCssSelector;
import org.openqa.selenium.support.FindBy;

public class HomePage {
	  
static By searchBox = By.cssSelector("#gh-ac");
static By searchIcon = By.id("gh-btn");
static By selectFirstItem = By.cssSelector("#srp-river-results-listing1 .s-item__title");
static By addToCart = By.cssSelector("#atcRedesignId_btn");
static By goToCart = By.cssSelector(".btn:nth-child(2)");

	  public static void openHomePage() {
		 
		 DriverInitializer.getDriver("chrome");
		 DriverInitializer.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 DriverInitializer.driver.get("https://www.ebay.com.au");
	      Log.info("URL launched");
	        
	    }

	  public static void searchBookAddToCart() {
		  
		  
			 DriverInitializer.driver.findElement(searchBox).sendKeys("book");
			 DriverInitializer.driver.findElement(searchIcon).click();
			 DriverInitializer.driver.findElement(selectFirstItem).click();
			 DriverInitializer.driver.findElement(addToCart).click();
			 DriverInitializer.driver.findElement(goToCart).click();
			 Log.info("Item added and navigated to cart");
		
	}

}
