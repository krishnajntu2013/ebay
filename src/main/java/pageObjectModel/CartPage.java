package pageObjectModel;

import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.internal.FindsByCssSelector;
import org.openqa.selenium.support.FindBy;

public class CartPage {
	  
static By cartQuantity = By.cssSelector("#gh-cart-n");
static By searchIcon = By.id("gh-btn");
static By remove_icon = By.cssSelector("span:nth-child(3) > .faux-link");
static By messageToUser = By.cssSelector(".font-title-3 > span > span");

public static void verifyItemAddedToCart() {
		 
		int cart_quanity_value = Integer.parseInt(DriverInitializer.driver.findElement(cartQuantity).getText());
		org.junit.Assert.assertEquals(1, cart_quanity_value);
		
		Log.info("item added to cart verified");
		
		
	}

public static void deleteItemfromCart() {
	DriverInitializer.driver.findElement(remove_icon).click();
	Log.info("item deleted to cart ");
		
	}

public static void verifyCartEmpty() {
		String message = DriverInitializer.driver.findElement(messageToUser).getText();
		org.junit.Assert.assertEquals("You don't have any items in your cart.", message);
		System.out.println("Item removed and cart empty");
		Log.info("item deleted to cart verified ");
	}

}
